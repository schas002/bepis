#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# Based on Kenneth Reitz' setup.py <https://github.com/kennethreitz/setup.py>
# Note: To use the 'upload' functionality of this file, you must install Twine:
# $ pip install twine

import os
import io
import json
import sys

from shutil import rmtree
from setuptools import find_packages, setup, Command

HERE = os.path.abspath(os.path.dirname(__file__))

with io.open(os.path.join(HERE, 'README.md'), encoding='UTF-8') as f:
	LONG_DESCRIPTION = '\n' + f.read()

PACKAGE_JSON = {}
with open(os.path.join(HERE, 'package.json')) as f:
	PACKAGE_JSON = json.load(f)

class PublishCommand(Command):
	"""Support setup.py publish."""
	description = 'Build and publish this package.'
	user_options = []
	@staticmethod
	def status(thing):
		"""Print thing in bold."""
		print('\033[1m{0}\033[0m'.format(thing))
	def initialize_options(self):
		pass
	def finalize_options(self):
		pass
	def run(self):
		try:
			self.status('Removing previous builds...')
			rmtree(os.path.join(HERE, 'dist'))
		except FileNotFoundError:
			pass
		self.status('Building Source and Wheel (universal) distribution...')
		os.system(
			'{0} setup.py sdist bdist_wheel --universal'.format(sys.executable)
		)
		self.status('Uploading the package to PyPi via Twine...')
		os.system('twine upload dist/*')
		sys.exit()

setup(
	name=PACKAGE_JSON['name'],
	version=PACKAGE_JSON['version'],
	description=PACKAGE_JSON['description'],
	long_description=LONG_DESCRIPTION,
	author=PACKAGE_JSON['author'],
	packages=find_packages(exclude=('tests',)),
	install_requires=PACKAGE_JSON['install_requires'],
	tests_require=PACKAGE_JSON['tests_require'],
	include_package_data=True,
	license=PACKAGE_JSON['license'],
	classifiers=PACKAGE_JSON['keywords'],
	cmdclass={
		'publish': PublishCommand
	}
)
