import os
import json

HERE = os.path.abspath(os.path.dirname(__file__))

PACKAGE_JSON = {}
with open(os.path.join(HERE, 'package.json')) as f:
	PACKAGE_JSON = json.load(f)

__version__ = PACKAGE_JSON['version']
