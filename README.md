# bepis

> A binky concept project

Coming soon at a bongle bebis near you...

## Install

bepis is *not* on PyPI, yet. Until there's a proper way to install:

```sh
$ py -3.6 setup.py install
```

When you want to uninstall, remove the egg/wheel installed by setup.py manually.

bepis is tested on Python 3.6.

## Maintainer

- Andrew Zyabin - @schas002 - [@zyabin101@botsin.space](https://botsin.space/@zyabin101)

## Contribute

Hnnggy! Every issue and PR is welcome.

Every contributor to this repository must follow the code of conduct, which is: don't be rude.

## License

[MIT](LICENSE) &copy; Andrew Zyabin
