# Changelog

All notable changes to this project will be documented in this file.

The format is based on version 1.0.0 of [Keep a Changelog](http://keepachangelog.com/en/1.0.0/) and this project adheres to version 2.0.0 of [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## Contents

- [Unreleased](#unreleased)

## [Unreleased][1]

### Added

+ A dev-friendly environment, with Pipenv, setup.py, package.json, Prospector, Coverage, pytest and Tox.

* * *

<small>

[1]: https://gitlab.com/schas002/bepis/tree/HEAD

</small>
